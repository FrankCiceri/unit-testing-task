﻿using static System.Runtime.InteropServices.JavaScript.JSType;

namespace UnitTesting_Task
{
    public static class Methods
    {
        public static int MaxCountUniqueConsecutiveChars(string? inputString)
        {
            if (inputString is null) throw new ArgumentNullException(nameof(inputString));
            

            int maxLen = 0;
            int currentLen = 0;

            for (int i = 1; i < inputString.Length; i++)
            {
                var currentChar = inputString[i];
                var previousChar = inputString[i - 1];
                if (currentChar != previousChar)
                {
                    currentLen++;
                }
                else
                {
                    if (currentLen > maxLen)
                    {
                        maxLen = currentLen;                        
                    }
                    currentLen = 0;
                }
            }

            return Math.Max(maxLen, currentLen);
        }

        public static int MaxCountEqualConsecutiveLatinChars(string inputString)
        {

            if (inputString is null) throw new ArgumentNullException(nameof(inputString));
            
            int maxLen = 0;
            int currentLen = 0;
            char? holdPreviousLatinChar = null;
            bool isLatinFunc(char x) => Char.IsLetter(x);

            for (int i = 1; i < inputString.Length; i++)
            {
                var currentChar = inputString[i];
                var previousChar = inputString[i - 1];
                var currentIsLatin = isLatinFunc(currentChar);
                var previousIsLatin = isLatinFunc(previousChar);
                
                if (currentChar != ' ')
                {
                    
                    if (currentIsLatin && currentChar == previousChar)
                    {
                            currentLen++;
                    }
                    else if (previousChar == ' ' && holdPreviousLatinChar is not null && currentChar == holdPreviousLatinChar) 
                    {
                        currentLen++;
                    }
                    else
                    {
                        if (currentLen > maxLen)
                        {
                            maxLen = currentLen;
                        }
                        currentLen = 0;
                    }
                }
                else if (previousIsLatin)
                {
                    holdPreviousLatinChar = previousChar;
                }

                
            }

            return Math.Max(maxLen, currentLen);
        }

        public static int MaxCountEqualConsecutiveDigits(string inputString)
        {

            if (inputString is null) throw new ArgumentNullException(nameof(inputString));

            int maxLen = 0;
            int currentLen = 0;
            char? holdPreviousDigitChar = null;
            bool isDigitFunc(char x) => Char.IsDigit(x);

            for (int i = 1; i < inputString.Length; i++)
            {
                var currentChar = inputString[i];
                var previousChar = inputString[i - 1];               
                var currentIsDigit = isDigitFunc(currentChar);
                var previousIsDigit = isDigitFunc(previousChar);

                if (currentChar != ' ')
                {
                    if (currentIsDigit && currentChar == previousChar)
                    {
                        currentLen++;
                    }
                    else if (previousChar == ' ' && holdPreviousDigitChar is not null && currentChar == holdPreviousDigitChar) 
                    {
                        currentLen++;
                    }
                    else
                    {
                        if (currentLen > maxLen)
                        {
                            maxLen = currentLen;
                        }
                        currentLen = 0;
                    }
                }else if(previousIsDigit)
                {
                    holdPreviousDigitChar = previousChar;
                }

            }

            return Math.Max(maxLen, currentLen);
        }



    }
}
