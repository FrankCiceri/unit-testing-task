
using UnitTesting_Task;

namespace MethodTests
{
    [TestFixture]
    public class ConsecutiveUniqueCharsTests
    {             

        [TestCaseSource(nameof(AllTestCases))]
        public void MaxCountUniqueConsecutiveChars_ReturnsValue(string line, int expected)
        {           
            
            //Act
            var actual = Methods.MaxCountUniqueConsecutiveChars(line);

            //Assert
            Assert.That(expected.Equals(actual));

        }

        [Test]
        public void MaxCountUniqueConsecutiveChars_ThrowsException()
        {
            //Arrange
            string? line = null;

            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => Methods.MaxCountUniqueConsecutiveChars(line));

        }
        //Initial Cases in Single Object
        //public static object[] ConsecutiveUniqueCharsCases =
        //{
        //new object[] { "", 0},
        //new object[] { "   ", 0},
        //new object[] { "a b c d e f g", 12 },
        //new object[] { "a", 0},
        //new object[] { "aaaaaa", 0 },
        //new object[] { "abbccddd", 1 },
        //new object[] { "Hello, Good Morning", 9 },
        //new object[] { "!@#$%^", 5 }, 
        //new object[] { "abc def", 6 },
        //new object[] { "123abc", 5 },
        //new object[] { "AaBbCc", 5 },
        //new object[] { new string('a', 1000), 0 }

        //};


        public static IEnumerable<object[]> EmptyAndSingleCharacterCases()
        {
            yield return new object[] { "", 0 };
            yield return new object[] { "a", 0 };
            yield return new object[] { "   ", 0 };
        }

        public static IEnumerable<object[]> UniqueCharacterCases()
        {
            yield return new object[] { "a b c d e f g", 12 };
            yield return new object[] { "abbccddd", 1 };

        }

        public static IEnumerable<object[]> MixedCharacterCases()
        {
            yield return new object[] { "Hello, Good Morning", 9 };
            yield return new object[] { "!@#$%^", 5 };
            yield return new object[] { "abc def", 6 };
            yield return new object[] { "abc!@#$%^def", 11 };
            yield return new object[] { "123abc", 5 };
            yield return new object[] { "AaBbCc", 5 };
        }

        public static IEnumerable<object[]> OtherCharCases()
        {
            yield return new object[] { new string('a', 1000), 0 };
            yield return new object[] { "1234567890", 9 };
        }
        public static IEnumerable<object[]> AllTestCases()
        {
            foreach (var testCase in EmptyAndSingleCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in UniqueCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in MixedCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in OtherCharCases())
            {
                yield return testCase;
            }
        }
    }
}