﻿using UnitTesting_Task;

namespace MethodTests
{
    [TestFixture]
    public class ConsecutiveEqualLatinCharsTests
    {
        [TestCaseSource(nameof(AllTestCases))]
        public void MaxCountEqualConsecutiveLatinChars_ReturnsValue(string line, int expected)
        {

            //Act
            var actual = Methods.MaxCountEqualConsecutiveLatinChars(line);

            //Assert
            Assert.That(expected.Equals(actual));

        }

        [Test]
        public void MaxCountEqualConsecutiveChars_ThrowsException()
        {
            //Arrange
            string? line = null;

            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => Methods.MaxCountEqualConsecutiveLatinChars(line));

        }

    //Initial Test Cases in Single object
    //    public static object[] ConsecutiveEqualLatinCharsCases =
    //    {
    //    new object[] { "", 0},
    //    new object[] { "a", 0},
    //    new object[] { "aaaaaa", 5 },
    //    new object[] { "abbccddd", 2 },
    //    new object[] { "Hello, Good Morning", 1 },
    //    new object[] { "Goodbyeeeee!", 4 },
    //    new object[] { "!@#$%^", 0 },
    //    new object[] { "aaa    aaa", 5 },
    //    new object[] { "aaa    Aaa", 2 },
    //    new object[] { "aaa!@#$%^aaa", 2 },
    //    new object[] { "aaa112233aaa", 2 },
    //    new object[] { " aaa112233aaa ", 2 },
    //    new object[] { "112233", 0 },
    //    new object[] { "AaBbCc", 0 },
    //    new object[] { new string('a', 1000), 999 },
    //    new object[] { "aA", 0 },
    //    new object[] { "aaaAAAaaa", 2 },        
    //    new object[] { "a b c d e f g", 0 }       
    //};


        public static IEnumerable<object[]> EmptyAndSingleCharacterCases()
        {
            yield return new object[] { "", 0 };
            yield return new object[] { "a", 0 };
            yield return new object[] { "A", 0 };
        }

        public static IEnumerable<object[]> ConsecutiveLatinCharacterCases()
        {
            yield return new object[] { "aaaaaa", 5 };
            yield return new object[] { "abbccddd", 2 };
            yield return new object[] { new string('a', 1000), 999 };
        }

        public static IEnumerable<object[]> MixedCharacterCases()
        {
            yield return new object[] { "Hello, Good Morning", 1 };
            yield return new object[] { "Goodbyeeeee!", 4 };
            yield return new object[] { "!@#$%^", 0 };            
            yield return new object[] { "aaa!@#$%^aaa", 2 };
            yield return new object[] { "aaa112233aaa", 2 };            
            yield return new object[] { "112233", 0 };
            yield return new object[] { "AaBbCc", 0 };
            yield return new object[] { "aA", 0 };
            yield return new object[] { "aaaAAAaaa", 2 };
            
        }

        public static IEnumerable<object[]> SpacedLatinCharsCases()
        {
            yield return new object[] { " aaa112233aaa ", 2 };
            yield return new object[] { "aaa    aaa", 5 };
            yield return new object[] { "aaa    Aaa", 2 };
            yield return new object[] { "a b c d e f g", 0 };

        }

        public static IEnumerable<object[]> AllTestCases()
        {
            foreach (var testCase in EmptyAndSingleCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in ConsecutiveLatinCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in MixedCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in SpacedLatinCharsCases())
            {
                yield return testCase;
            }
        }
    }
}
