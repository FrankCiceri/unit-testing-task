﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTesting_Task;

namespace MethodTests
{
    [TestFixture]
    public class ConsecutiveEqualDigitCharsTests
    {
        [TestCaseSource(nameof(AllTestCases))]
        public void MaxCountEqualConsecutiveLatinChars_ReturnsValue(string line, int expected)
        {

            //Act
            var actual = Methods.MaxCountEqualConsecutiveDigits(line);

            //Assert
            Assert.That(expected.Equals(actual));

        }

        [Test]
        public void MaxCountEqualConsecutiveChars_ThrowsException()
        {
            //Arrange
            string? line = null;

            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => Methods.MaxCountEqualConsecutiveDigits(line));

        }
        //Initial Cases in single object
        //    public static object[] ConsecutiveEqualDigitCharsCases =
        //    {
        //    new object[] { "", 0},
        //    new object[] { "1", 0},       
        //    new object[] { "Goodby33333!", 4 },
        //    new object[] { "!@#$%^", 0 },
        //    new object[] { "111    111", 5 },
        //    new object[] { "111    111    111", 8 },
        //    new object[] { "111    222", 2 },
        //    new object[] { "111!@#$%^111", 2 },
        //    new object[] { "aaa1122333aaa", 2 },
        //    new object[] { " aaa1122333aaa ", 2 },
        //    new object[] { "112233",  1},
        //    new object[] { "A1a1B1b1C1c1", 0 },
        //    new object[] { new string('1', 1000), 999 },
        //    new object[] { "1 2 3 4 5 6 7", 0 },
        //    new object[] { "1 1 1 1 1 1 1", 6 },
        //    new object[] { "1234567890", 0 },
        //    new object[] { "    1234    ", 0 },
        //    new object[] { "1122 3344 5566", 1 },
        //};

        public static IEnumerable<object[]> EmptyAndSingleCharacterCases()
        {
            yield return new object[] { "", 0 };
            yield return new object[] { "1", 0 };
            yield return new object[] { "a", 0 };
        }

        public static IEnumerable<object[]> DigitsOnlyCases()
        {
            yield return new object[] { "123456", 0 };
            yield return new object[] { "111111", 5 };
            yield return new object[] { "112233", 1 };
            yield return new object[] { new string('1', 1000), 999 };
        }

        public static IEnumerable<object[]> MixedCharacterCases()
        {
            yield return new object[] { "Goodby33333!", 4 };
            yield return new object[] { "1 2 3 4 5 6 7", 0 };
            yield return new object[] { "11a11b11", 1 };
            yield return new object[] { "111!@#$%^111", 2 };
            yield return new object[] { "aaa1122333aaa", 2 };
            yield return new object[] { " aaa1122333aaa ", 2 };
        }

        public static IEnumerable<object[]> SpacedDigitsCases()
        {                     
            yield return new object[] { "111    111", 5 };      
            yield return new object[] { "111    111    111", 8 };
            yield return new object[] { "1122 3344 5566", 1 };
        }

        public static IEnumerable<object[]> AllTestCases()
        {
            foreach (var testCase in EmptyAndSingleCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in DigitsOnlyCases())
            {
                yield return testCase;
            }

            foreach (var testCase in MixedCharacterCases())
            {
                yield return testCase;
            }

            foreach (var testCase in SpacedDigitsCases())
            {
                yield return testCase;
            }
        }
    }
}
